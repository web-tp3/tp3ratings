<?php

/*
 * This file is part of the web-tp3/tp3-shop.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace Tp3\Tp3ratings\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Tp3\Tp3ratings\Renderer\RatingsProvider;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Http\DispatcherInterface;
use TYPO3\CMS\Core\Http\NullResponse;
use TYPO3\CMS\Core\Http\Response;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class PageRequestMiddleware
 */
class PageRequestMiddleware implements MiddlewareInterface
{
    /**
     * Process page request
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Server\RequestHandlerInterface $handler
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if (isset($request->getServerParams()['REQUEST_URI']) && ($request->getServerParams()['REQUEST_URI'] == '/?eIDTp3=rating'
             ||  $request->getServerParams()['REQUEST_URI'] == '/?eIDTp3=rating&noCache=1')
             || (isset($request->getAttributes()['routing']) && $request->getAttributes()['routing']->getPageType() == '1337' )) {
            $rating = GeneralUtility::makeInstance(RatingsProvider::class)->processRequest($request);
            return $rating;

        }
        if (isset($request->getServerParams()['REQUEST_URI']) && (strpos($request->getServerParams()['REQUEST_URI'],  '/?eIDTp3=review')> 0
                ||  strpos($request->getServerParams()['REQUEST_URI'], '?eIDTp3=review') > 0
            || (isset($request->getAttributes()['routing']) && $request->getAttributes()['routing']->getPageType() == '1338' ))) {

            $rating = GeneralUtility::makeInstance(RatingsProvider::class)->processRequest($request);
            return $rating;
//            $eID = $request->getParsedBody()['eIDTp3'] ?? $request->getQueryParams()['eIDTp3'] ?? null;
//
//            if ($eID === null) {
//                return $handler->handle($request);
//            }
//
//            // Remove any output produced until now
//            ob_clean();
//
//            /** @var Response $response */
//            $response = new Response();
//
//            if (empty($eID) || !isset($GLOBALS['TYPO3_CONF_VARS']['FE']['eIDTp3_include'][$eID])) {
//                return $response->withStatus(404, 'eIDTp3 not registered');
//            }
//            $this->dispatcher = GeneralUtility::makeInstance(DispatcherInterface::class);
//            $configuration = $GLOBALS['TYPO3_CONF_VARS']['FE']['eIDTp3_include'][$eID];
//
//            // Simple check to make sure that it's not an absolute file (to use the fallback)
//            if (strpos($configuration, '::') !== false || is_callable($configuration)) {
//                $request = $request->withAttribute('target', $configuration);
//                return $this->dispatcher->dispatch($request) ?? new NullResponse();
//            }
        }
        return $handler->handle($request);
    }



//    /**
//     * @param DispatcherInterface $dispatcher
//     */
//    public function __construct(DispatcherInterface $dispatcher)
//    {
//        $this->dispatcher = $dispatcher;
//    }

//    /**
//     * Dispatches the request to the corresponding eID class or eID script
//     *
//     * @param ServerRequestInterface $request
//     * @param RequestHandlerInterface $handler
//     * @return ResponseInterface
//     * @throws Exception
//     */
//    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
//    {
//        $eID = $request->getParsedBody()['eIDTp3'] ?? $request->getQueryParams()['eIDTp3'] ?? null;
//
//        if ($eID === null) {
//            return $handler->handle($request);
//        }
//
//        // Remove any output produced until now
//        ob_clean();
//
//        /** @var Response $response */
//        $response = new Response();
//
//        if (empty($eID) || !isset($GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['tp3ratings']['eIDTp3_include'][$eID])) {
//            return $response->withStatus(404, 'eIDTp3 not registered');
//        }
//
//        $configuration = $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['tp3ratings']['eIDTp3_include'][$eID];
//
//        // Simple check to make sure that it's not an absolute file (to use the fallback)
//        if (strpos($configuration, '::') !== false || is_callable($configuration)) {
//            $request = $request->withAttribute('target', $configuration);
//            return $this->dispatcher->dispatch($request) ?? new NullResponse();
//        }
//        return new NullResponse();
//    }
}
