<?php

/*
 * This file is part of the web-tp3/tp3_guestbook.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace Tp3\Tp3ratings\Service;

use Tp3\Tp3ratings\Domain\Repository\EntryRepository;
use Tp3\Tp3ratings\Session\FrontendSessionStorage;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Frontend\Page\PageRepository;

class GuestbookService implements SingletonInterface
{

    /**
     * @var PageRepository
     */
    protected $pageRepository = null;
    /**
     * @param PageRepository $PageRepository
     */
    public function injectPageRepository(PageRepository $PageRepository)
    {
        $this->pageRepository = $PageRepository;
    }
    /**
     * @var \Tp3\Tp3ratings\Domain\Repository\EntryRepository
     */
    protected $entryRepository = null;

    /**
     * @param EntryRepository $EntryRepository
     */
    public function injectEntryRepository(EntryRepository $EntryRepository)
    {
        $this->entryRepository = $EntryRepository;
    }
    /**
     * @var FrontendSessionStorage
     */
    private $sessionContainer;

    /**
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     */
    protected $configurationManager;

    /**
     * @var array
     * @api
     */
    protected $settings;

    public function __construct()
    {
    }

    /**
     * @param \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager
     * @return void
     */
    public function injectConfigurationManager(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager)
    {
        $this->configurationManager = $configurationManager;
        $this->settings = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS, 'tp3shop');
    }

    /**
     * @return FrontendSessionStorage
     */
    protected function getSessionContainer()
    {
        if (is_null($this->sessionContainer)) {
            $this->sessionContainer = new FrontendSessionStorage('Tp3\Tp3ratings\Service\GuestbookService');
        }
        return $this->sessionContainer;
    }

    /**
     * @return int
     */
    public function getCurrentPageUid()
    {
        /* @var $feController TypoScriptFrontendController */
        $feController = $GLOBALS['TSFE'];
        $currentPageUid = $feController->id;
        return $currentPageUid;
    }

    /**
     * @return EntryRepository
     */
    public function getEntryRepository()
    {
        return $this->entryRepository;
    }

    /**
     * @return Entry[]
     */
    public function getEntries()
    {
        return $this->entryRepository->findAll()->toArray();
    }
}
