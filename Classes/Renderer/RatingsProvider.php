<?php

/*
 * This file is part of the web-tp3/tp3mods.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace Tp3\Tp3ratings\Renderer;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Configuration\SiteConfiguration;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Exception\Page\PageNotFoundException;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Http\Response;
use TYPO3\CMS\Core\Routing\PageArguments;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\RootlineUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/***************************************************************

/**
 *
 */
class RatingsProvider implements SingletonInterface
{

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * ratingsdataRepository
     *
     * @var \Tp3\Tp3ratings\Domain\Repository\RatingsdataRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $ratingsdataRepository = null;

    /**
     * view
     *
     * @var \Tp3\Tp3ratings\View\Rating
     *
     */
    protected $view = null;
    /**
     * IplogRepository
     *
     * @var \Tp3\Tp3ratings\Domain\Repository\IplogRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */

    /**
     * @var FrontendSessionStorage
     */
    protected $_sessionStorage;

    /**
     * @var Site[]
     */
    protected $sites = [];

    /**
     * Short-hand to quickly fetch a site based on a rootPageId
     *
     * @var array
     */
    protected $mappingRootPageIdToIdentifier = [];

    /**
     * @var SiteConfiguration
     */
    protected $siteConfiguration;

    /**
     * @var ServerRequestInterface
     */
    protected $request;

    /**
     * @var array Arguments
     */
    protected $arguments;

    /**
     * Fetches all existing configurations as Site objects
     *
     * @param SiteConfiguration $siteConfiguration
     */
    public function __construct()
    {
//        parent::__construct();
        $this->siteConfiguration = GeneralUtility::makeInstance(SiteConfiguration::class);
        $this->fetchAllSites();
    }

    /**
     * Fetches the content and builds a content file out of it
     *
     * @param ServerRequestInterface $request the current request object
     * @return ResponseInterface the modified response
     */
    public function processRequest(ServerRequestInterface $request): ResponseInterface
    {
        $this->request = $request;
        $params = $request->getQueryParams();
        if (count($params)> 0 &&  $params['eIDTp3'] == 'rating' && isset( $params['ratingdata'])) {
            //&& $this->resolveActionMethodName() == "ratingAction"
            $data_str = $params['ratingdata'];
            if ($params['check']) {
                if ($params['check'] == md5($params['ref'] . $params['rating'] . $data_str . $GLOBALS['TYPO3_CONF_VARS']['SYS']['encryptionKey'])) {
                   // $request->SetArgument('check', $params['check']);
                    $params['ratingdata'] =  unserialize(base64_decode($data_str));
                }
                //  throw InvalidPropertyException;
            } else {
                // throw PageNotFoundException;
            }
        }
        if (count($params)> 0 &&   $params['eIDTp3'] == 'review' &&  isset( $params['tp3reviewdata'])) {
            //&& $this->resolveActionMethodName() == "ratingAction"
            $this->tp3reviewdata = $params['tp3reviewdata'];
            // $request->SetArgument("ratingdata", unserialize(base64_decode($data_str)));
            if (isset( $params['check'])) {
              //   $params['ratingdata'] =  unserialize(base64_decode($data_str));
//   $request->SetArgument('check', $params['check']);
            } else {
                //  throw PageNotFoundException;
                return (new Response())->withStatus(404);

            }
        }
        $this->arguments = $params;
        try {
            /** @var Response $response */
            $response = new Response();
            $response->getBody()->write($this->Rate());
            return $response;
        } catch (\InvalidArgumentException $e) {

            // add a 410 "gone" if invalid parameters given
            return (new Response())->withStatus(410);
        } catch (Exception $e) {
            return (new Response())->withStatus(404);
        }
    }

    /**
     * Initialize the typoscript frontend controller
     *
     * @param int $pid
     *
     * @return void
     */
    private function getTypoScriptFrontendController(int $pid = 0)
    {

        $site = $this->getSiteByPageId($pid);
        $pageId = $site ? $site->getRootPageId(): ($GLOBALS['TSFE'] ? $GLOBALS['TSFE']->id : 3);

            /** @var \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $frontend */

        $GLOBALS['TSFE']  =  GeneralUtility::makeInstance(
            TypoScriptFrontendController::class,
            GeneralUtility::makeInstance(Context::class),
            $site,
            $site->getDefaultLanguage(),
            new PageArguments((int)$pageId, '0', [])
        );
        $GLOBALS['TSFE']->fe_user =  GeneralUtility::makeInstance(FrontendUserAuthentication::class);


        $GLOBALS['TSFE']->newCObj();//        \TYPO3\CMS\Frontend\Utility\EidUtility::initLanguage();

        return  $GLOBALS['TSFE'];
    }

    /**
     * action review
     *
     *
     * @return void
     */
    public function Review()
    {

        if (preg_match('/^\d{2,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/', $_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $remoteaddress =  $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $remoteaddress =$_SERVER['REMOTE_ADDR'];
        }
        $this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        $this->ratingsdataRepository =  $this->objectManager->get(\Tp3\Tp3ratings\Domain\Repository\RatingsdataRepository::class);
        $this->iplogRepository =   $this->objectManager->get(\Tp3\Tp3ratings\Domain\Repository\IplogRepository::class);


        if ($GLOBALS['TSFE']->loginUser) {
            $iplog = $this->iplogRepository->findFeUserEmail($GLOBALS['TSFE']->fe_user->email, $GLOBALS['TSFE']->page['uid'])->getFirst();
        }
        if (!$iplog instanceof \Tp3\Tp3ratings\Domain\Model\Iplog) {
            $iplog = $this->iplogRepository->findbyIpandRef($remoteaddress, $GLOBALS['TSFE']->page['uid'], $GLOBALS['TSFE']->fe_user->id)->getFirst();
        }

        if (!$iplog instanceof \Tp3\Tp3ratings\Domain\Model\Iplog) {
            $iplog = $this->objectManager->get('Tp3\\Tp3ratings\\Domain\\Model\\Iplog');
            $iplog->setIp($remoteaddress);
            if ($iplog->ref->getObj() == 'pages') {
                $iplog->setPid($this->request->hasArgument('ref') ? $this->request->getArgument('ref') :  $GLOBALS['TSFE']->page['uid']);
            } else {
                $iplog->setPid($GLOBALS['TSFE']->domainStartPage ?  $GLOBALS['TSFE']->domainStartPage :  $GLOBALS['TSFE']->page['uid']);
            }
        }
        $iplog->ref->setReviewCount($iplog->ref->getReviewCount()+1);
        $iplog->setReview((is_string($this->tp3reviewdata['review']) ? $this->tp3reviewdata['review'] : ''));
        $iplog->setUserid((is_string($this->tp3reviewdata['emailadresse']) ? $this->tp3reviewdata['emailadresse'] : ''));
        $iplog->SetSession($GLOBALS['TSFE']->fe_user->id);
        /*
         *   $GLOBALS['TSFE']->fe_user->user = $GLOBALS['TSFE']->fe_user->fetchUserSession();
                    $GLOBALS['TSFE']->loginUser = 1;
         */
        if ($GLOBALS['TSFE']->loginUser) {
            $iplog->setCruserId($GLOBALS['TSFE']->fe_user->getUid());
        }

        if (intval($iplog->getUid())> 0) {
            $this->ratingsdataRepository->update($iplog->ref);
            $this->iplogRepository->update($iplog);
        } else {
            //  $this->ratingsdataRepository->add($ratingsdata);
            $this->iplogRepository->add($iplog);
        }
        //$ratingsdata->setSubmittext($this->controllerContext->getFlashMessageQueue());
        $this->persistenceManager = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $this->persistenceManager->persistAll();

//        /** @var \TYPO3\CMS\Fluid\View\StandaloneView $Ratings */
//        $infoWindowView = $this->objectManager->get('TYPO3\\CMS\\Fluid\\View\\StandaloneView');
//
//        // Dateiformat festlegen
//        $infoWindowView->setFormat('json');
//        //$this->conf = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
//
//        // Typoscript-Konfiguration fuer entsprechendes Template holen
//        $templateRootPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($this->conf['view']['templateRootPath']);
//
//        // Template-Pfad festlegen bzw. entsprechend anpassen
//        $templatePathAndFilename = $templateRootPath . 'Ratingsdata/Rating.json';
//
//        if (\TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getExtensionVersion('extbase')) < 8007000) {
//            $infoWindowView->setTemplatePathAndFilename($templatePathAndFilename);
//        } else {
//            $layoutRootPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($this->conf['view']['layoutRootPath']);
//            $partialRootPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($this->conf['view']['partialRootPath']);
//            //   $infoWindowView->setRenderingContext()
//            $infoWindowView->setLayoutRootPaths([$templateRootPath . 'Ratingsdata/Layouts/', $layoutRootPath]);
//            $infoWindowView->setPartialRootPaths([$partialRootPath]);
//            $infoWindowView->setTemplatePathAndFilename($templatePathAndFilename);
//        }
//        //	$infoWindowView->setLayoutName("Rating");
//        // Objekt �bergeben und Template verarbeiten
//        $infoWindowView->assign('disableReview', $this->settings['disableReview']);
//        $infoWindowView->assign('ratingsdata', json_encode($this->tp3reviewdata));
//        $infoWindowView->assign('iplog', json_encode($iplog));
//        $infoWindowView->assign('settings', $this->settings);
//        // Rendern und zurueckgeben
//        $infoWindow = $infoWindowView->render();
        return json_encode($this->tp3reviewdata);
    }

    public function Rate()
    {
        if (preg_match('/^\d{2,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/', $_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $remoteaddress =  $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $remoteaddress =$_SERVER['REMOTE_ADDR'];
        }
        $this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        $this->ratingsdataRepository =  $this->objectManager->get(\Tp3\Tp3ratings\Domain\Repository\RatingsdataRepository::class);
        $this->iplogRepository =   $this->objectManager->get(\Tp3\Tp3ratings\Domain\Repository\IplogRepository::class);

        if ($GLOBALS['TSFE']->loginUser) {
            $iplog = $this->iplogRepository->findFeUserEmail($GLOBALS['TSFE']->fe_user->email, $GLOBALS['TSFE']->page['uid'])->getFirst();
        }

        if (!$iplog instanceof \Tp3\Tp3ratings\Domain\Model\Iplog) {
            $iplog = $this->iplogRepository->findbyIpandRef($remoteaddress, $GLOBALS['TSFE']->page['uid'], $GLOBALS['TSFE']->fe_user->id)->getFirst();


        }

        if (!$iplog instanceof \Tp3\Tp3ratings\Domain\Model\Iplog) {
            $iplog = $this->objectManager->get('Tp3\\Tp3ratings\\Domain\\Model\\Iplog');
            $iplog->setIp($remoteaddress);
            if ($iplog->ref->getObj() == 'pages') {
                $iplog->setPid($this->arguments['ref'] ? $this->arguments['ref'] :  $GLOBALS['TSFE']->page['uid']);
            } else {
                $iplog->setPid($GLOBALS['TSFE']->domainStartPage ?  $GLOBALS['TSFE']->domainStartPage :  $GLOBALS['TSFE']->page['uid']);
            }
        }
        if (!$iplog->ref instanceof \Tp3\Tp3ratings\Domain\Model\Ratingsdata) {
            $this->ratingsdataRepository->add($iplog->ref);
        }
        else{
            $this->ratingsdataRepository->add($iplog->ref);
        }
        $iplog->ref->setReviewCount($iplog->ref->getReviewCount()+1);
        $iplog->setReview((is_string($this->tp3reviewdata['review']) ? $this->tp3reviewdata['review'] : ''));
        $iplog->setUserid((is_string($this->tp3reviewdata['emailadresse']) ? $this->tp3reviewdata['emailadresse'] : ''));
        $iplog->SetSession($GLOBALS['TSFE']->fe_user->id);
        /*
         *   $GLOBALS['TSFE']->fe_user->user = $GLOBALS['TSFE']->fe_user->fetchUserSession();
                    $GLOBALS['TSFE']->loginUser = 1;
         */
        if ($GLOBALS['TSFE']->loginUser) {
            $iplog->setCruserId($GLOBALS['TSFE']->fe_user->getUid());
        }

        if (intval($iplog->getUid())> 0) {
            $this->iplogRepository->update($iplog);
        } else {
            //  $this->ratingsdataRepository->add($ratingsdata);
            $this->iplogRepository->add($iplog);
        }
        //$ratingsdata->setSubmittext($this->controllerContext->getFlashMessageQueue());
        $this->persistenceManager =  $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $this->persistenceManager->persistAll();

       return json_encode($iplog->ref->_getCleanProperties(), JSON_THROW_ON_ERROR , 2048);

    }

    /**
     * Find a site by given identifier
     *
     * @param string $identifier
     * @return Site
     * @throws SiteNotFoundException
     */
    public function getSiteByIdentifier(string $identifier): Site
    {
        if (isset($this->sites[$identifier])) {
            return $this->sites[$identifier];
        }
        throw new SiteNotFoundException('No site found for identifier ' . $identifier, 1521716628);
    }

    /**
     * Traverses the rootline of a page up until a Site was found.
     *
     * @param int $pageId
     * @param array $rootLine
     * @param string|null $mountPointParameter
     * @return Site
     * @throws SiteNotFoundException
     */
    public function getSiteByPageId(int $pageId, array $rootLine = null, string $mountPointParameter = null): Site
    {
        if ($pageId === 0) {
            // page uid 0 has no root line. We don't need to ask the root line resolver to know that.
            $rootLine = [];
        }
        if (!is_array($rootLine)) {
            try {
                $rootLine = GeneralUtility::makeInstance(RootlineUtility::class, $pageId, $mountPointParameter)->get();
            } catch (PageNotFoundException $e) {
                // Usually when a page was hidden or disconnected
                // This could be improved by handing in a Context object and decide whether hidden pages
                // Should be linkable too
                $rootLine = [];
            }
        }
        foreach ($rootLine as $pageInRootLine) {
            if (isset($this->mappingRootPageIdToIdentifier[(int)$pageInRootLine['uid']])) {
                return $this->sites[$this->mappingRootPageIdToIdentifier[(int)$pageInRootLine['uid']]];
            }
        }
        throw new SiteNotFoundException('No site found in root line of page ' . $pageId, 1521716622);
    }

    /**
     * @param bool $useCache
     */
    protected function fetchAllSites(bool $useCache = true): void
    {
        $this->sites = $this->siteConfiguration->getAllExistingSites($useCache);
        foreach ($this->sites as $identifier => $site) {
            $this->mappingRootPageIdToIdentifier[$site->getRootPageId()] = $identifier;
        }
    }

}
