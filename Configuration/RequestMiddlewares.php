<?php

/*
 * This file is part of the web-tp3/tp3-shop.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */
if(class_exists(\Tp3\Tp3ratings\Middleware\PageRequestMiddleware::class)){

    return [
        'frontend' => [
            'tp3/tp3-ratings-page-request' => [
                'target' => \Tp3\Tp3ratings\Middleware\PageRequestMiddleware::class,
                'after' => [
                    'typo3/cms-frontend/tsfe',
                    'typo3/cms-frontend/authentication',
                    'typo3/cms-frontend/backend-user-authentication',
                    'typo3/cms-frontend/site',
                ]
            ]
        ]
    ];
}
