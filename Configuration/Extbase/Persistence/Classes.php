<?php
declare(strict_types = 1);
/*
 *
plugin.tx_extbase {
    persistence {
        classes {
             Tp3\Tp3ratings\Domain\Model\Iplog {
		        mapping {
		          tableName = tx_tp3ratings_domain_model_iplog
		          recordType = Tp3\Tp3ratings\Domain\Model\Iplog
		             columns {
                            ref.mapOnProperty = Tp3\Tp3ratings\Domain\Model\Ratinsgdata
						 	crdate.mapOnProperty = crdate
                     }
		         }
		         subclasses {
                                     Tp3\Tp3ratings\Domain\Model\Ratinsgdata = Tp3\Tp3ratings\Domain\Model\Ratinsgdata
                  }
		      }

             Tp3\Tp3ratings\Domain\Model\Ratinsgdata {
                mapping {
                    tableName = tx_tp3ratings_domain_model_ratingsdata
                    recordType = Tp3\Tp3ratings\Domain\Model\Ratingsdata
                    columns {

                    }
                }

            }

        }
    }
}
 */

if (class_exists(\Tp3\Tp3ratings\Domain\Model\Iplog::class)) {


    return [
        \Tp3\Tp3ratings\Domain\Model\Iplog::class => [
            'subclasses' => [
                \Tp3\Tp3ratings\Domain\Model\Ratingsdata::class
            ]
        ],
        \Tp3\Tp3ratings\Domain\Model\Iplog::class => [
            'tableName' => 'tx_tp3ratings_domain_model_iplog',
            'properties' => [
                'uid' => [
                    'fieldName' => 'ref'
                ],
                'crdate' => [
                    'fieldName' => 'crdate'
                ],
            ],
        ],
        \Tp3\Tp3ratings\Domain\Model\Ratingsdata::class => [
            'tableName' => 'tx_tp3ratings_domain_model_ratingsdata',
            'recordType' => 0,
        ],
    ];

}
else{
    return [


    ];
}
